<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500|Racing+Sans+One" rel="stylesheet">
<link rel="stylesheet" href="/public/main.bundle.css">
<!--Import Font Awesome-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />