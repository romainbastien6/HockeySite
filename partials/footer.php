<footer>
    <div>
        <ul>
            <li><a href="/">Accueil</a></li>
            <li><a href="/results">Résultats</a></li>
            <li><a href="/news">Actualités</a></li>
            <li><a href="/medias">Médias</a></li>
            <li><a href="/teams">Equipes</a></li>
            <li><a href="/join-us">Rejoignez-nous</a></li>
        </ul>
    </div>
    <div>
        <i class="fab fa-facebook-square"></i>
        <i class="fab fa-twitter-square"></i>
        <i class="fab fa-instagram"></i>
    </div>
</footer>
<script src="/public/bundle.js"></script>