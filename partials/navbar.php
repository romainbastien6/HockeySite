<nav id="navbar">
    <a href="/">
        <div>
            <img src="https://res.cloudinary.com/djhf37mbt/image/upload/v1534278210/hockey-site/site/logo.png" alt="">
            <span>RAPR</span>
        </div>
    </a>
    <i class="fas fa-bars" id="hamburger"></i>
    <div id="nav_list">
        <a href="/results">résultats</a>
        <a href="/news">actualités</a>
        <a href="/medias">médias</a>
        <a href="/teams">équipes</a>
        <a href="/joinus">rejoignez-nous</a>
    </div>
</nav>