<?php
require_once("model/DataManager.php");
require_once('classes/Video.php');


class VideosManager extends DataManager
{
    public function index()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, yt_id FROM videos');
        $videos_raw = $req->fetchAll(PDO::FETCH_ASSOC);
        $videos = array();
        foreach ($videos_raw as $video_data)
        {
            $video = new Video($video_data);
            array_push($videos, $video);
        }
        return $videos;
    }

    public function showLastVideo()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, yt_id FROM videos ORDER BY id DESC LIMIT 1');
        $video_raw = $req->fetch(PDO::FETCH_ASSOC);
        $video = new Video($video_raw);
        return $video;
    }

    public function indexJSON()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, yt_id FROM videos');
        $videos = $req->fetchAll(PDO::FETCH_ASSOC);
        return $videos;
    }

    public function create($video)
    {
        $db = $this->dbConnect();
        $title = $video->title();
        $yt_id = $video->yt_id();
        $req = $db->prepare('INSERT INTO videos VALUES (DEFAULT, ?, ?)');
        $req->execute(array($title, $yt_id));

    }

    public function delete($video_id)
    {
        $db = $this->dbConnect();
        $db->query('DELETE FROM videos WHERE id = ' . $video_id);
    }

    public function update($video){
        $db = $this->dbConnect();
        $req = $db->prepare('UPDATE videos SET title = ?, yt_id = ? WHERE id = ?');
        $req->execute(array($video['title'], $video['yt_id'], $video['id']));
    }
}