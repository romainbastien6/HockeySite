<?php
require_once("model/DataManager.php");
require ('classes/News.php');

class NewsManager extends DataManager
{

    public function index()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, description, category, featured_image_url, body FROM news');
        $req = $req->fetchAll(PDO::FETCH_ASSOC);

        $news = array();
        foreach($req as $article)
        {
            $news_instance = new News($article);
            array_push($news, $news_instance);
        }
        return $news;
    }

    public function indexJSON(){
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, description, category, featured_image_url, body FROM news');
        $req = $req->fetchAll(PDO::FETCH_ASSOC);

        return $req;
    }

    public function indexByCategory($category)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('SELECT id, title, description, category, featured_image_url, body FROM news WHERE category = ?');
        $req->execute(array($category));
        $req = $req->fetchAll(PDO::FETCH_ASSOC);
        $news = array();
        foreach($req as $article)
        {
            $news_instance = new News($article);
            array_push($news, $news_instance);
        }
        return $news;
    }

    public function indexLimited($limit)
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title, description, category, featured_image_url, body FROM news LIMIT ' . $limit);
        $req = $req->fetchAll(PDO::FETCH_ASSOC);
        $news = array();
        foreach($req as $article)
        {
            $news_instance = new News($article);
            array_push($news, $news_instance);
        }
        return $news;
    }

    public function show($newsId)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('SELECT id, title, description, category, featured_image_url, body FROM news WHERE id = ?');
        $req->execute(array($newsId));
        $post = $req->fetch(PDO::FETCH_ASSOC);

        $post = new News($post);

        return $post;
    }

    public function create($news)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('INSERT INTO news VALUES (DEFAULT, ?, ?, ?, ?, ?)');
        $req->execute(array($news->title(), $news->category(), $news->description(), $news->featured_image_url(), $news->body()));
        return true;
    }

    public function update($news)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('UPDATE news SET id = ?, title = ?, category = ?, description = ?, featured_image_url = ?, body = ? WHERE id = ?');
        $req->execute(array($news->id(), $news->title(), $news->category(), $news->description(), $news->featured_image_url(), $news->body(), $news->id()));
        return true;
    }

    public function delete($news_id)
    {
        $db = $this->dbConnect();
        $db->query('DELETE FROM news WHERE id = ' . $news_id);
    }
}

