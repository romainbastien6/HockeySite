<?php
require_once('model/DataManager.php');
require_once ('classes/Gallery.php');

class ImagesManager extends DataManager
{
    public function index()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title FROM galleries ORDER BY id');
        $req = $req->fetchAll();
        $galleries = array();
        foreach ($req as &$gallery) {
            $db2 = $this->dbConnect();
            $req2 = $db2->prepare('SELECT img_url FROM images WHERE galleries_id = ?');
            $images = $req2->execute(array($gallery['id']));
            $images = $req2->fetchAll(PDO::FETCH_ASSOC);
            $gallery = new Gallery(array('id' => $gallery['id'], 'title' => $gallery['title']));
            $gallery->setImages($images);
            array_push($galleries, $gallery);
        }
        return $galleries;
    }

    public function indexLimited($limit)
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title FROM galleries ORDER BY id DESC LIMIT ' . $limit);
        $req = $req->fetchAll();
        $galleries = array();
        foreach ($req as &$gallery) {
            $db2 = $this->dbConnect();
            $req2 = $db2->prepare('SELECT img_url FROM images WHERE galleries_id = ?');
            $images = $req2->execute(array($gallery['id']));
            $images = $req2->fetchAll(PDO::FETCH_ASSOC);
            $gallery = new Gallery(array('id' => $gallery['id'], 'title' => $gallery['title']));
            $gallery->setImages($images);
            array_push($galleries, $gallery);
        }
        return $galleries;
    }

    public function indexJSON()
    {
        $db = $this->dbConnect();
        $req = $db->query('SELECT id, title FROM galleries ORDER BY id DESC');
        $req = $req->fetchAll(PDO::FETCH_ASSOC);
        $galleries = array();
        foreach ($req as &$gallery) {
            $db2 = $this->dbConnect();
            $req2 = $db2->prepare('SELECT id, img_url FROM images WHERE galleries_id = ?');
            $images = $req2->execute(array($gallery['id']));
            $images = $req2->fetchAll(PDO::FETCH_ASSOC);
            $gallery['images'] = $images;
            array_push($galleries, $gallery);
        }
        return $galleries;
    }

    public function createGallery($gallery)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('INSERT INTO galleries VALUES (DEFAULT, ?)');
        $req->execute(array($gallery->title()));

    }

    public function updateGallery($gallery)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('UPDATE galleries SET title = ? WHERE id = ? ');
        $req->execute(array($gallery['title'], $gallery['id']));
    }

    public function addImageToGallery($gallery_id, $imageURL)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('INSERT INTO images VALUES (DEFAULT, ?, ?)');
        $req->execute(array($imageURL, $gallery_id));
    }

    public function deleteGallery($gallery_id)
    {
        $db = $this->dbConnect();
        $req = $db->query('DELETE FROM galleries WHERE id = ' . $gallery_id);
    }

    public function deleteImage($image_id)
    {
        $db = $this->dbConnect();
        $req = $db->query('DELETE FROM images WHERE id = ' . $image_id);
    }
}