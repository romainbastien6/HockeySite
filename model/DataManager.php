<?php
require __DIR__ . '/../vendor/autoload.php';

class DataManager
{
    public function dbConnect()
    {
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
        $db = new PDO('mysql:host=localhost;dbname=hockeysite;charset=utf8', $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
        return $db;
    }
}