<?php
require_once("model/DataManager.php");

class ResultsManager extends DataManager
{
    public function index(){
        $db = $this->dbConnect();
        $games = $db->query('SELECT * FROM games');
        $games = $games->fetchAll(PDO::FETCH_ASSOC);
        return $games;
    }


    public function showByTeam($team_id)
    {
        $db = $this->dbConnect();
        $completed_games = $db->query('SELECT * FROM games WHERE team_id = ' . $team_id . ' AND completed = TRUE ORDER BY date DESC');
        $upcoming_games = $db->query('SELECT * FROM games WHERE team_id = ' . $team_id . ' AND completed = FALSE ORDER BY date');
        $completed_games= $completed_games->fetchAll(PDO::FETCH_ASSOC);
        $upcoming_games = $upcoming_games->fetchAll(PDO::FETCH_ASSOC);

        $res["playedGames"] = $completed_games;
        $res["upcomingGames"] = $upcoming_games;

        $standings = $db->query('SELECT teamName, playedGames, points, goalFor, goalAgainst, goalFor - goalAgainst AS goalaverage FROM standings WHERE category = ' . $team_id . ' ORDER BY points DESC, goalaverage DESC');
        $standings = $standings->fetchAll(PDO::FETCH_ASSOC);

        $res["standing"] = $standings;


        return $res;
    }

    public function showByTeamLimited($team_id)
    {
        $db = $this->dbConnect();
        $completed_games = $db->query('SELECT * FROM games WHERE team_id = ' . $team_id . ' AND completed = TRUE ORDER BY date DESC LIMIT 3');
        $upcoming_games = $db->query('SELECT * FROM games WHERE team_id = ' . $team_id . ' AND completed = FALSE ORDER BY date LIMIT 3');
        $completed_games= $completed_games->fetchAll(PDO::FETCH_ASSOC);
        $upcoming_games = $upcoming_games->fetchAll(PDO::FETCH_ASSOC);

        $res["playedGames"] = $completed_games;
        $res["upcomingGames"] = $upcoming_games;

        $standings = $db->query('SELECT teamName, playedGames, points, goalFor, goalAgainst, goalFor - goalAgainst AS goalaverage FROM standings WHERE category = ' . $team_id . ' ORDER BY points DESC, goalaverage DESC');
        $standings = $standings->fetchAll(PDO::FETCH_ASSOC);

        $res["standing"] = $standings;


        return $res;
    }


    public function createGame($game)
    {
        $db = $this->dbConnect();

        $req = $db->prepare('INSERT INTO games VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?)');

        if ($game['completed']){
            $req->execute(array($game['date'], $game['team_id'], $game['gameType'], $game['team1'], $game['team2'], $game['completed'], $game['score1'], $game['score2']));
        }
        else {
            $req->execute(array($game['date'], $game['team_id'], $game['gameType'], $game['team1'], $game['team2'], $game['completed'], NULL, NULL));
        }
    }

    public function updateGame($game)
    {
        $db = $this->dbConnect();

        $req = $db->prepare('UPDATE games SET date = ?, team_id = ?, gameType = ?, team1 = ?, team2 = ?, completed = ?, score1 = ?, score2 = ? WHERE id = ?');

        if ($game['completed']){
            $req->execute(array($game['date'], $game['team_id'], $game['gameType'], $game['team1'], $game['team2'], $game['completed'], $game['score1'], $game['score2'], $game['id']));
        }
        else {
            $req->execute(array($game['date'], $game['team_id'], $game['gameType'], $game['team1'], $game['team2'], $game['completed'], NULL, NULL, $game['id']));
        }
    }

    public function deleteGame($game_id){
        $db = $this->dbConnect();

        $db->query('DELETE FROM games WHERE id = ' . $game_id);
    }

    public function getTeamStandings($team_id)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('SELECT id, teamName, playedGames, points, goalFor, goalAgainst, goalFor - goalAgainst AS goalaverage FROM standings WHERE category = ? ORDER BY points DESC, goalaverage DESC');
        $req->execute(array($team_id));
        $res = $req->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    public function addTeamStandings($team)
    {
        $db = $this->dbConnect();
        $req = $db->prepare('INSERT INTO standings VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)');
        $req->execute(array($team["category"], $team["teamName"], $team["playedGames"], $team["points"], $team["goalFor"], $team["goalAgainst"]));
        $id = $db->query('SELECT * FROM standings ORDER BY id DESC LIMIT 1');
        return $id->fetch(PDO::FETCH_ASSOC);
    }

    public function updateTeamStandings($team){
        $db = $this->dbConnect();
        $req = $db->prepare('UPDATE standings SET teamName = ?, playedGames = ?, points = ?, goalFor = ?, goalAgainst = ? WHERE id = ?');
        $req->execute(array($team["teamName"], $team["playedGames"], $team["points"], $team["goalFor"], $team["goalAgainst"], $team["id"]));
    }

    public function deleteTeamStandings($team_id){
        $db = $this->dbConnect();
        $req = $db->query('DELETE FROM standings WHERE id = ' . $team_id);
    }
}