<?php
require_once("model/DataManager.php");

class TeamsManager extends DataManager
{
    public function index(){
        $db = $this->dbConnect();
        $team = $db->query('SELECT * FROM teams');
        $team = $team->fetchAll(PDO::FETCH_ASSOC);
        return $team;
    }

    public function indexPlayers()
    {
        $db = $this->dbConnect();
        $players = $db->query('SELECT * FROM players');
        $players = $players->fetchAll(PDO::FETCH_ASSOC);
        foreach ($players as &$player)
        {
            $palmares = $db->query('SELECT id, content FROM palmares WHERE player_id = ' . $player["id"]);
            $palmares = $palmares->fetchAll(PDO::FETCH_ASSOC);
            $player["palmares"] = $palmares;
        }
        return $players;
    }

    public function show($team_id)
    {
        $db = $this->dbConnect();
        $team = $db->query('SELECT * FROM teams WHERE id = ' . $team_id);
        $team = $team->fetch(PDO::FETCH_ASSOC);

        $players = $db->query('SELECT id, name, position, picture, experience FROM players WHERE team1_id = ' . $team_id . ' OR team2_id = ' . $team_id);
        $players = $players->fetchAll(PDO::FETCH_ASSOC);

        foreach ($players as &$player)
        {
            $palmares = $db->query('SELECT id, content FROM palmares WHERE player_id = ' . $player["id"]);
            $palmares = $palmares->fetchAll(PDO::FETCH_ASSOC);
            $player["palmares"] = $palmares;
        }

        $team['players'] = $players;

        return $team;

    }

    public function createPlayer($player)
    {
        $db = $this->dbConnect();

        $req = $db->prepare('INSERT INTO players VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)');
        $req->execute(array($player['name'], $player['team1_id'], $player['team1_id'], $player['position'], $player['picture'], $player['experience']));

        $player_id = $db->query('SELECT id FROM players ORDER BY id DESC LIMIT 1');
        $player_id = $player_id->fetch(PDO::FETCH_ASSOC);
        $player_id = $player_id['id'];

        foreach($player['palmares'] as $palmares)
        {
            $req2 = $db->prepare('INSERT INTO palmares VALUES (DEFAULT, ?, ?)');
            $req2->execute(array($player_id, $palmares['content']));
        }

    }

    public function updatePlayer($player)
    {
        $db = $this->dbConnect();

        $req = $db->prepare('UPDATE players SET name = ?, team1_id = ?, team2_id = ?, position = ?, picture = ?, experience = ? WHERE id = ?');
        $req->execute(array($player['name'], $player['team1_id'], $player['team1_id'], $player['position'], $player['picture'], $player['experience'], $player['id']));
    }

    public function addPalmares($content, $player_id)
    {
        $db = $this->dbConnect();

        $req = $db->prepare('INSERT INTO palmares VALUES (DEFAULT, ?, ?)');
        $req->execute(array($player_id, $content));
    }

    public function deletePalmares($id)
    {
        $db = $this->dbConnect();

        $req = $db->query('DELETE FROM palmares WHERE id = ' . $id);
    }
}