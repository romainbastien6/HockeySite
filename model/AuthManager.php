<?php
require './vendor/autoload.php';
require_once('model/DataManager.php');

class AuthManager extends DataManager {


    public function emailConfirmation()
    {
        $db = $this->dbConnect();
        $auth = new \Delight\Auth\Auth($db);

        try {
            $auth->confirmEmailAndSignIn($_GET['selector'], $_GET['token']);

            echo "Email has been verified";
            echo $_SESSION['signed_in'];
            // email address has been verified
        }
        catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
            // invalid token
        }
        catch (\Delight\Auth\TokenExpiredException $e) {
            // token expired
        }
        catch (\Delight\Auth\UserAlreadyExistsException $e) {
            // email address already exists
        }
        catch (\Delight\Auth\TooManyRequestsException $e) {
            // too many requests
        }
    }

    public function logIn($email, $password)
    {
        $db = $this->dbConnect();
        $auth = new \Delight\Auth\Auth($db);
        try {
            $auth->login($email, $password);

            // user is logged in
        }
        catch (\Delight\Auth\InvalidEmailException $e) {
            // wrong email address
        }
        catch (\Delight\Auth\InvalidPasswordException $e) {
            // wrong password
        }
        catch (\Delight\Auth\EmailNotVerifiedException $e) {
            // email not verified
        }
        catch (\Delight\Auth\TooManyRequestsException $e) {
            // too many requests
        }
    }
}