    <?php $page_title = 'Photos'; ?>

    <?php ob_start(); ?>

    <div id="photos_page">

        <header class="default-header" style="background-image: url('http://res.cloudinary.com/djhf37mbt/image/upload/e_blur:392/v1534257108/hockey-site/site/medias.jpg');">
            <div>
                <h1>Photos</h1>
            </div>
        </header>

        <main>

            <div class="thumbnails-container">

                <?php foreach ($galleries as $gallery)
                {
                ?>

                <div class="thumbnail">
                    <a href="<?=  $gallery->_images[0]["img_url"] ?>" data-fancybox="<?=  $gallery->id() ?>">
                        <figure>
                            <img src="<?=  $gallery->_images[0]["img_url"] ?>" alt="">
                            <figcaption><?=  $gallery->title() ?></figcaption>
                        </figure>
                    </a>

                    <?php foreach ($gallery->_images as $image) { ?>

                    <a href="<?= $image["img_url"] ?>" data-fancybox="<?= $gallery->id() ?>"></a>

                    <?php } ?>
                </div>

                <?php } ?>

            </div>

        </main>

    </div>

    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>

