<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Equipes</title>
    <?php require('partials/CSSimports.php'); ?>
</head>
<body id="teams_page">

    <?php require('partials/navbar.php'); ?>

    <header class="default-header" style="">
        <div>
            <h1>équipes</h1>
        </div>
    </header>

    <main>
        <div id="teams-app"></div>
    </main>

    <?php require('partials/footer.php'); ?>

</body>
</html>