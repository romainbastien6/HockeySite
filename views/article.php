    <?php
        require './vendor/autoload.php';
        $Parsedown = new Parsedown();
        $page_title = $post->title();
        $page_id = 'article_page'
    ?>

    <?php ob_start(); ?>

    <div>
        <header style="background-image: url('<?= $post->featured_image_url() ?>');"></header>

        <main>

            <article>
                <h1><?= $post->title() ?></h1>

                <strong><p><?= $post->description() ?></p></strong>

                <p><?= $Parsedown->text($post->body()) ?></p>


            </article>

            <div id="news_container">

                <?php
                foreach ($news as $article)
                {
                ?>

                    <a href="news/<?= $article->id() ?>">
                        <figure class="news_item">
                            <img src="<?= $article->featured_image_url() ?>" alt="">
                            <figcaption class="news_info">
                                <h5><?= $article->title() ?></h5>
                                <p><?= $article->description() ?></p>
                            </figcaption>
                        </figure>
                    </a>

                <?php
                }
                ?>

            </div>

        </main>
    </div>

    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>