    <?php $page_title = ''; ?>

    <?php ob_start(); ?>

    <header class="default-header" style="background: url('/assets/resized_news_header.jpg');">
        <div>
            <h1>Médias</h1>
        </div>
    </header>

    <main>
        <form action="/login" method="post">
            <div>
                <input type="email" name="email" placeholder="Email">
            </div>

            <div>
                <input type="password" name="password" placeholder="Mot de passe">
            </div>

            <div>
                <button type="submit">Se connecter</button>
            </div>

        </form>
    </main>
    
                
    <?php $content  = ob_get_clean(); ?>

    <?php require('template.php'); ?>