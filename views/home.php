<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hockey Site</title>
    <?php require('partials/CSSimports.php'); ?>
</head>
<body id="index_page">

    <?php require('partials/navbar.php'); ?>
    <header>
        <main>
            <div>
                <h1>En route vers une nouvelle saison</h1>
                <p>
                    Rejoignez-nous pour cette nouvelle saison, riche en aventure : un titre de champions à reconquérir,
                    de nouveaux joueurs, de nouveaux objectifs ...
                </p>
                <a href="/joinus"><button>Rejoignez l'aventure</button></a>
            </div>
        </main>
    </header>

    <main>

        <section id="news">

            <a href="/news">
                <h2>Actualités</h2>
            </a>

            <div id="news_container">

                <?php
                foreach ($news as $post)
                {
                    ?>

                    <a href="news/<?= $post->id() ?>">
                        <figure class="news_item">
                            <img src="<?= $post->featured_image_url() ?>" alt="">
                            <figcaption class="news_info">
                                <h5><?= $post->title() ?></h5>
                                <p><?= $post->description() ?></p>
                            </figcaption>
                        </figure>
                    </a>
                    <?php
                }
                ?>


            </div>

            <a href="news.php"><button>Voir plus</button></a>

        </section>


        <section>
            <a href="/results">
                <h2>résultats</h2>
            </a>
            <div id="results-app"></div>
            <button>Voir plus</button>
        </section>

        <section id="medias">

            <h2>Médias</h2>

            <div>

                <section id="photos">
                    <h3>Photos</h3>

                    <div class="thumbnails-container">

                        <?php foreach ($galleries as $gallery)
                        {
                            ?>

                            <div class="thumbnail">
                                <a href="<?=  $gallery->_images[0]["img_url"] ?>" data-fancybox="<?=  $gallery->id() ?>">
                                    <figure>
                                        <img src="<?=  $gallery->_images[0]["img_url"] ?>" alt="">
                                        <figcaption><?=  $gallery->title() ?></figcaption>
                                    </figure>
                                </a>

                                <?php foreach ($gallery->_images as $image) {?>

                                    <a href="<?=  $image["img_url"] ?>" data-fancybox="<?=  $gallery->id() ?>"></a>

                                    <?php
                                }
                                ?>
                            </div>

                        <?php } ?>

                    </div>


                </section>

                <section id="videos">
                    <h3>Vidéos</h3>
                    <div class="video-container">
                        <iframe src="https://www.youtube.com/embed/<?= $video->yt_id() ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>

                </section>

            </div>

            <button>Voir plus</button>

        </section>

    </main>


    <?php require('partials/footer.php'); ?>

</body>
</html>