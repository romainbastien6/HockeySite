<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rejoignez-nous</title>
    <?php require('partials/CSSimports.php'); ?>
</head>
<body id="joinus_page">

<?php require('partials/navbar.php'); ?>

<header class="default-header" style="background-image: url('http://res.cloudinary.com/djhf37mbt/image/upload/e_blur:392/v1534257811/joinus.jpg');">
    <div>
        <h1>Rejoignez-nous</h1>
    </div>
</header>

<main>
    <p>Sautez le pas, rejoignez nous</p>

    <p>Remplissez ce formulaire pour recevoir notre dossier d'inscription par mail</p>

    <form action="" method="post">

        <div class="form_item">
            <label for="name">Nom</label>
            <input type="text" name="name" id="name">
        </div>

        <div class="form_item">
            <label for="first_name">Prénom</label>
            <input type="text" name="first_name" id="first_name">
        </div>

        <div class="form_item">
            <label for="birth_date">Date de naissance</label>
            <input type="date" id="birth_date" name="birth_date">
        </div>

        <div class="form_item">
            <label for="mail">Adresse Mail</label>
            <input type="email" name="email" id="email">
        </div>

        <div class="form_item">
            <label for="member_type">Vous souhaitez nous rejoindre en tant que :</label>
            <select name="member_type" id="member_type">
                <option value="player">Joueur</option>
                <option value="volunteer">Bénévole</option>
            </select>
        </div>

        <button type="submit">Envoyer moi le dossier</button>

    </form>
</main>


<?php require('partials/footer.php'); ?>

</body>
</html>