    <?php $page_title = 'Vidéos'; ?>

    <?php ob_start(); ?>

    <div id="videos_page">

        <header class="default-header" style="">
            <div>
                <h1>Vidéos</h1>
            </div>
        </header>

        <main>

            <div class="thumbnails-container">


                <?php
                foreach ($videos as $video)
                {
                ?>

                    <div class="thumbnail">
                        <a data-fancybox href="https://www.youtube.com/watch?v=<?= $video->yt_id() ?>">
                            <figure>
                                <img src="https://i.ytimg.com/vi/<?= $video->yt_id() ?>/hqdefault.jpg" alt="">
                                <figcaption>
                                    <p><?= $video->title() ?></p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>

                <?php
                }
                ?>


            </div>

        </main>

    </div>

    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>