    <?php $page_title = 'Actualités'; ?>

    <?php ob_start(); ?>

    <div id="news_page">

        <header class="default-header" style="background-image: url('http://res.cloudinary.com/djhf37mbt/image/upload/e_blur:392/v1534254953/hockey-site/site/news.jpg');">
            <div>
                <h1>Actualités</h1>
            </div>
        </header>

        <main>
            <section>

                <nav class="subnav">
                    <a href="/news">dernières nouvelles</a>
                    <a href="/news/debrief">débrief'</a>
                    <a href="/news/evenements">évènements</a>
                    <a href="/news/vie_du_club">vie du club</a>
                </nav>

                <div id="news_container">

                    <?php
                    foreach ($news as $post)
                    {
                        ?>

                        <a href="news/<?= $post->id() ?>">
                            <figure class="news_item">
                                <img src="<?= $post->featured_image_url() ?>" alt="">
                                <figcaption class="news_info">
                                    <h5><?= $post->title() ?></h5>
                                    <p><?= $post->description() ?></p>
                                </figcaption>
                            </figure>
                        </a>
                    <?php
                    }
                    ?>
                </div>
            </section>
        </main>

    </div>
                
    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>

    
                
  
