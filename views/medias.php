    <?php $page_title = 'Médias'; ?>

    <?php ob_start(); ?>

    <div id="medias_page">

        <header class="default-header" style="background-image: url('http://res.cloudinary.com/djhf37mbt/image/upload/e_blur:392/v1534257108/hockey-site/site/medias.jpg');">
            <div>
                <h1>Médias</h1>
            </div>
        </header>


        <main>

            <section id="medias_type">
                <div>
                    <div>
                        <a class="layer" href="/medias/images">
                            <h3>Photos</h3>
                        </a>
                    </div>
                    <div>
                        <a class="layer" href="/medias/videos">
                            <h3>Vidéos</h3>
                        </a>
                    </div>
                </div>
            </section>

        </main>

    </div>

    <?php $content = ob_get_clean(); ?>

    <?php require('template.php'); ?>
