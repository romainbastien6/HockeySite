<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $page_title ?></title>
    <?php require('partials/CSSimports.php'); ?>
</head>
<body id="<?= $page_id ?>">

<?php require('partials/navbar.php'); ?>

<?= $content ?>

<?php require('partials/footer.php'); ?>

</body>
</html>