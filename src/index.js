import $ from 'jquery'
window.jQuery = $;
require("@fancyapps/fancybox");

import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

import Results from './results_app/Results'
import Teams from './teams_app/Teams'



$(document).ready(function(){

    const menu = $("#nav_list");
    const hamburger = $("#hamburger");
    let menuOpen = false;

    $(document).scroll(function () {
        const $nav = $("#navbar");
        $nav.toggleClass('plain_nav', $(this).scrollTop() > $nav.height());
    });

    function openMenu(){
        menu.css("left", "0px");
        menuOpen = true;
    }

    function closeMenu(){
        menu.css("left", "-320px");
        menuOpen = false;
    }

    function toggleMenu(){
        if (menuOpen) {
            closeMenu();
        } else {
            openMenu();
        }
    }

    hamburger.on({
        mouseenter: function(){
            openMenu();
        }
    });

    menu.on({
        mouseleave: function(){
            closeMenu();
        }

    });

    hamburger.on({
        click: function(){
            toggleMenu();
        }
    })



});

const body_id = $('body').attr("id")



switch (body_id) {
    case 'teams_page':
        ReactDOM.render(<Teams />, document.getElementById("teams-app"));
        break

    case 'index_page':
    case 'results_page':
        ReactDOM.render(<Results />, document.getElementById("results-app"));
        break

    default:
        console.log('')
}

