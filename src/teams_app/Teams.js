import React from 'react'
import axios from 'axios'

import TeamPanel from './TeamPanel'

class Teams extends React.Component {

    constructor() {
        super();
        this.state = null
    }


    componentWillMount() {
        axios.get('/api/teams/1')
            .then((response) => {
                this.setState(response.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    setTeam(team_id) {
        axios.get('/api/teams/' + team_id)
            .then((response) => {
                this.setState(response.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    render() {

        if(!this.state) {
            return <div>Loading</div>
        }
        else {
            return (
                <div>
                    <div className="team-selector" id="team-selector">
                        <span onClick={ () => {this.setTeam('1')} }>Elite</span>
                        <span onClick={ () => {this.setTeam('2')} }>Prénational</span>
                        <span onClick={ () => {this.setTeam('3')} }>Junior</span>
                        <span onClick={ () => {this.setTeam('4')} }>Cadet</span>
                        <span onClick={ () => {this.setTeam('5')} }>Minime</span>
                        <span onClick={ () => {this.setTeam('6')} }>Benjamin</span>
                        <span onClick={ () => {this.setTeam('7')} }>Poussin</span>
                    </div>

                    <TeamPanel players={this.state.players} picture={this.state.picture}/>

                </div>
            )
        }
    }
}

module.exports = Teams