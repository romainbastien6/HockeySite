import React from 'react'

const TeamPhoto = (props) => {
    return (
        <img src={props.imageURL}/>
    )
}

module.exports = TeamPhoto