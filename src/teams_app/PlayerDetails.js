import React from 'react'

const PlayerDetails = (props) => {
    return (
        <div id="player_details">
            <img src={props.player.picture} alt=""/>
            <div>
                <h4>{props.player.name}</h4>
                <p id="player_position">{props.player.position}</p>
                <p>{props.player.experience} saisons</p>
                <ul>
                    {
                        props.player.palmares.map((achievement) =>
                            <li key={achievement.id}>{achievement.content}</li>
                        )
                    }
                </ul>
            </div>
        </div>
    )
}

module.exports = PlayerDetails