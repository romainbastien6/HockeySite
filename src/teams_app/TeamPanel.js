import React from 'react'

import TeamPhoto from './TeamPhoto'
import PlayerList from './PlayerList'
import PlayerDetails from './PlayerDetails'

class TeamPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            players: this.props.players,
            activePlayer: this.props.players[0]
        }
        this.changeActivePlayer = this.changeActivePlayer.bind(this)
    }

    componentDidUpdate(){
        if (this.props.players !== this.state.players) {
            this.setState({
                players: this.props.players,
                activePlayer: this.props.players[0]
            });
        }
    }

    changeActivePlayer(player_id) {
        const newActivePlayer = this.state.players.find(player => player.id === player_id)
        this.setState({
            players: this.props.players,
            activePlayer: newActivePlayer
        })
    }

    render() {
        return (

            <div id="team_panel">

                <TeamPhoto imageURL={this.props.picture}/>

                <div id="players">

                    <PlayerDetails player={this.state.activePlayer}/>

                    <PlayerList players={this.state.players} changeActivePlayer={this.changeActivePlayer}/>

                </div>

            </div>

        )
    }

}

module.exports = TeamPanel