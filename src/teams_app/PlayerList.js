import React from 'react'

class PlayerList extends React.Component {

    constructor(props) {
        super(props);
        this.changeActivePlayer = this.changeActivePlayer.bind(this)
    }

    changeActivePlayer(player_id) {
        this.props.changeActivePlayer(player_id);
    }

    render() {
        return <div id="player_list">
            <ul>
                {
                    this.props.players.map((player) =>
                        <li key={player.id} onClick={() => {this.changeActivePlayer(player.id)}}>{player.name}</li>
                    )
                }
            </ul>
        </div>
    }

}

module.exports = PlayerList