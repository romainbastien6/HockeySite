import $ from 'jquery'
window.jQuery = $;

const palmaresHandler = () => {
    $(document).ready(()=>{
        const hiddenDiv = $('#palmaresHidden');
        const list = $('#palmares');
        const input = $('#newPalmares');
        const button = $('#addPalmares');

        button.on({
            click: () => {
                let hidden = document.createElement('input');
                hidden.setAttribute('type', "hidden");
                hidden.setAttribute('name', "palmares[]");
                $(hidden).val(input.val());
                hiddenDiv.append(hidden);
                let li = document.createElement("li");
                $(li).text(input.val());
                list.append(li);
                input.val('')
            }
        })
    })


}

module.exports = palmaresHandler;