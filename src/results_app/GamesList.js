import React from 'react'

import GameItem from './GameItem'

const GamesList = (props) => {

    return <div className="games_list">

        {
            props.games.map((game) =>
                <GameItem game={game} key={game.id}/>
            )
        }

    </div>

}

module.exports = GamesList