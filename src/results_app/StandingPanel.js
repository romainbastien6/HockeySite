import React from 'react'

import StandingItem from './StandingItem'

const StandingPanel = (props) => {

    return (
        <div id="standing_panel">
            <h3>Classement</h3>

            <table>
                <tbody>

                    <tr>
                        <td></td>
                        <td>MJ</td>
                        <td>Pts</td>
                        <td>BP</td>
                        <td>BC</td>
                        <td>+/-</td>
                    </tr>

                    {
                        props.standing.map((item) =>
                            <StandingItem item={item} key={item.teamName}/>
                        )
                    }

                </tbody>

            </table>
        </div>
    )

}

module.exports = StandingPanel