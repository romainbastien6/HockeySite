import React from 'react'

const StandingItem = (props) => {

    const item = props.item

    return (
        <tr>
            <td>{item.teamName}</td>
            <td>{item.playedGames}</td>
            <td>{item.points}</td>
            <td>{item.goalFor}</td>
            <td>{item.goalAgainst}</td>
            <td>{item.goalFor - item.goalAgainst}</td>
        </tr>
    )

}

module.exports = StandingItem