import React from 'react'
import GamesList from './GamesList'

const GamesPanel = (props) => {

    return <div id="games_panel">

                <div>
                    <h3>Derniers Matchs</h3>
                    <GamesList games={props.playedGames}/>
                </div>

                <div>
                    <h3>Prochains Matchs</h3>
                    <GamesList games={props.upcomingGames}/>
                </div>

            </div>
}

module.exports = GamesPanel