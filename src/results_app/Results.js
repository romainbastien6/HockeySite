import React from 'react'
import axios from 'axios'

import GamesPanel from './GamesPanel'
import StandingPanel from './StandingPanel'

class Results extends React.Component {

    constructor() {
        super();
        this.state = null
        this.url = document.location.pathname

        if(this.url === '/') {
            this.api = '/api/home/results/'
        }
        else {
            this.api = '/api/results/'
        }
    }


    componentWillMount() {
        axios.get(this.api + '1')
            .then((response) => {
                this.setState(response.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    setTeam(team) {
        axios.get(this.api + team)
            .then((response) => {
                this.setState(response.data)
            })
            .catch((e) => {
                console.log(e)
            })
    }

    render() {

        let app;

        if (this.state) {
            app = <div id="results">

                <div className="team-selector" id="team-selector">
                    <span onClick={ () => {this.setTeam(1)} }>Elite</span>
                    <span onClick={  () => {this.setTeam(2)} }>Prénational</span>
                    <span onClick={  () => {this.setTeam(3)} }>Junior</span>
                    <span onClick={  () => {this.setTeam(4)} }>Cadet</span>
                    <span onClick={  () => {this.setTeam(5)} }>Minime</span>
                    <span onClick={  () => {this.setTeam(6)} }>Benjamin</span>
                    <span onClick={  () => {this.setTeam(7)} }>Poussin</span>
                </div>


                <div>

                    <GamesPanel playedGames={this.state.playedGames} upcomingGames={this.state.upcomingGames}/>

                    <StandingPanel standing={this.state.standing}/>
                </div>

            </div>
        } else {
            app = <div>Loading</div>
        }


        return <div>{app}</div>
    }
}

module.exports = Results