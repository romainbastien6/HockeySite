import React from 'react'

const GameItem = (props) => {

    return <div className="game_item">

        <p className="game_type">{props.game.gameType}</p>

        <div className="team_score">
            <span className="team_name">{props.game.team1}</span>
            <span className="score">{props.game.score1}</span>
        </div>

        <div className="team_score">
            <span className="team_name">{props.game.team2}</span>
            <span className="score">{props.game.score2}</span>
        </div>

        <p className="date">{props.game.date}</p>

    </div>

}

module.exports = GameItem