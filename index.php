<?php
require("./controllers/StaticController.php");
require("./controllers/NewsController.php");
require("./controllers/MediasController.php");
require("./controllers/ResultsController.php");
require("./controllers/TeamsController.php");
require("./controllers/AuthController.php");
require("./controllers/AdminController.php");
require("./controllers/HomeController.php");


require './vendor/autoload.php';
require_once ('./model/DataManager.php');

$request_uri = $_SERVER['REQUEST_URI'];



switch ($request_uri) {
    // Home page
    case '/':
        indexHome();
        break;
    case (preg_match('/\/api\/home\/results\/(\d+)/', $request_uri) ? true : false):
        preg_match('/\/api\/home\/results\/(\d+)/', $request_uri, $team_id);
        showByTeamLimited($team_id[1]);
        break;

    // News
    case '/news':
        indexNews();
        break;


    case (preg_match('/\/news\/(\d+)/', $request_uri) ? true : false):
        preg_match('/\/news\/(\d+)/', $request_uri, $id);
        showNews($id[1]);
        break;

    case (preg_match('/\/news\/([a-z_]+)/', $request_uri) ? true : false):
        preg_match('/\/news\/([a-z_]+)/', $request_uri, $category);
        switch ($category[1]) {
            case 'vie_du_club':
                indexNewsByCategory('Vie du Club');
                break;
            case 'evenements':
                indexNewsByCategory('Evènements');
                break;
            case 'debrief':
                indexNewsByCategory("Débrief'");
                break;
            default:
                echo '404 not found';
                break;
        }
        break;

    // Medias
    case '/medias':
        medias();
        break;

    case '/medias/videos':
        indexVideos();
        break;

    case '/medias/images':
        indexImages();
        break;

    //results
    case '/results':
        results();
        break;

    case (preg_match('/\/api\/results\/(\d+)/', $request_uri) ? true : false):
        preg_match('/\/api\/results\/(\d+)/', $request_uri, $team_id);
        showByTeam($team_id[1]);
        break;

    //teams
    case '/teams':
        teams();
        break;

    case (preg_match('/\/api\/teams\/(\d+)/', $request_uri) ? true : false):
        preg_match('/\/api\/teams\/(\d+)/', $request_uri, $team_id);
        showTeam($team_id[1]);
        break;

    //Join Us
    case '/joinus':
        joinus();
        break;



    case '/admin/news':
        authOkta('adminNews');
        break;

    case '/admin/update-news':
        authOkta('updateNews');
        break;

    case '/admin/delete-news':
        authOkta('deleteNews');
        break;

    case '/admin/add-news':
        authOkta('addNews');
        break;





    case '/admin/videos':
        authOkta('adminVideo');
        break;


    case '/admin/add-video':
        authOkta('addVideo');
        break;

    case '/admin/update-video':
        authOkta('updateVideo');
        break;

    case '/admin/delete-video':
        authOkta('deleteVideo');
        break;






    case '/admin/galleries':
        authOkta('indexGalleries');
        break;

    case '/admin/add-gallery':
        authOkta('addGallery');
        break;

    case '/admin/update-gallery':
        authOkta('updateGallery');
        break;

    case '/admin/add-image':
        authOkta('addImageToGallery');
        break;

    case '/admin/delete-gallery':
        authOkta('deleteGallery');
        break;

    case '/admin/delete-image':
        authOkta('deleteImage');
        break;

    case '/admin/players':
        authOkta('adminPlayers');
        break;

    case '/admin/update-player':
        authOkta('updatePlayer');
        break;

    case '/admin/add-palmares':
        authOkta('addPalmares');
        break;

    case '/admin/delete-palmares':
        authOkta('deletePalamares');
        break;

    case '/admin/add-player':
        authOkta('addPlayer');
        break;


    case '/admin/games':
        authOkta('adminGames');
        break;

    case '/admin/add-game':
        authOkta('addGame');
        break;

    case '/admin/update-game':
        authOkta('updateGame');
        break;

    case '/admin/delete-game':
        authOkta('deleteGame');
        break;

    case '/admin/standings':
        authOkta('adminStandings');
        break;

    case (preg_match('/\/admin\/get-team-standing\/(\d+)/', $request_uri) ? true : false):
        preg_match('/\/admin\/get-team-standing\/(\d+)/', $request_uri, $team_id);
        getStandingsTeam($team_id[1]);
        break;

    case '/admin/add-team-standings':
        authOkta('addStandingsTeam');
        break;

    case '/admin/update-team-standings':
        authOkta('updateStandingsTeam');
        break;

    case '/admin/delete-team-standings':
        authOkta('deleteStandingsTeam');
        break;





    default:
        echo '404 not found';
        break;
}