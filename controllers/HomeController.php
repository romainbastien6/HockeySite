<?php

require_once ('model/VideosManager.php');
require_once ('model/ImagesManager.php');
require_once('model/NewsManager.php');


function indexHome(){
    $imagesManager = new ImagesManager();
    $videosManager = new VideosManager();
    $newsManager = new NewsManager();

    $galleries = $imagesManager->indexLimited(6);
    $video = $videosManager->showLastVideo();
    $news = $newsManager->indexLimited(4);

    /*$payload = array(
        'galleries' => $galleries,
        'video' => $video,
        'news' => $news
    );*/

    require('./views/home.php');
}