<?php
require './vendor/autoload.php';

function authOkta($callback, $payload = null)
{

// Don't do anything for prefetch requests.
    if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ) {
        error_log('OPTIONS is here');
        return false;
    }

// Make sure the authorization header is available, if not return 401.
    if (!isset( $_SERVER['HTTP_AUTHORIZATION'] ) ) {
        error_log('wrong authorization header');
        return http_response_code( 401 );
    }

    $authType = null;
    $authData = null;

// Extract the auth type and the data from the Authorization header.
    list( $authType, $authData ) = explode( " ", $_SERVER['HTTP_AUTHORIZATION'], 2 );

// If the Authorization Header is not a bearer token, return a 401.
    if ( $authType != 'Bearer' ) {
        error_log('not bearer');
        return http_response_code( 401 );
    }

    try {
        // Setup the JWT Verifier.
        $jwtVerifier = ( new \Okta\JwtVerifier\JwtVerifierBuilder() )
            ->setAdaptor( new \Okta\JwtVerifier\Adaptors\SpomkyLabsJose() )
            ->setAudience('api://default')
            ->setClientId( '0oafwtx44obVRxBpn0h7' )
            ->setIssuer( 'https://dev-265389.oktapreview.com/oauth2/default' )
            ->build();

        // Verify the JWT from the Authorization Header.
        $jwt = $jwtVerifier->verify( $authData );

        if (is_null($payload)){
            $callback();
        }
        else {
            $callback($payload);
        }


    } catch (\Exception $e) {
        // We encountered an error, return a 401.
        error_log('wrong token');
        error_log($authData);

        http_response_code( 401 );
        die('Unauthorized');
    }

}
