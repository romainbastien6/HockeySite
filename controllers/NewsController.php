<?php
require_once('model/NewsManager.php');

function indexNews()
{
    $newsManager = new NewsManager();
    $news = $newsManager->index();
    require('./views/news.php');
}

function indexNewsByCategory($category)
{
    $newsManager = new NewsManager();
    $news = $newsManager->indexByCategory($category);

    require('./views/news.php');
}

function showNews($id)
{
    $newsManager = new NewsManager();
    $post = $newsManager->show($id);
    $news = $newsManager->indexLimited(3);
    require('./views/article.php');

}