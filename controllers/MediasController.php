<?php
require ('./model/VideosManager.php');
require ('./model/ImagesManager.php');

function indexVideos()
{
    $videoManager = new VideosManager();
    $videos = $videoManager->index();


    require('./views/videos.php');
}

function indexImages()
{
    $imagesManager = new ImagesManager();
    $galleries = $imagesManager->index();

    //echo json_encode($galleries);
    require('./views/photos.php');
}