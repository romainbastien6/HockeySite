<?php

require_once ('./model/ResultsManager.php');

function showByTeam($team_id)
{
    $resultsManager = new ResultsManager();
    $res = $resultsManager->showByTeam($team_id);
    $res = json_encode($res);

    echo $res;
}

function showByTeamLimited($team_id)
{
    $resultsManager = new ResultsManager();
    $res = $resultsManager->showByTeamLimited($team_id);
    $res = json_encode($res);

    echo $res;
}