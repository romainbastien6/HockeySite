<?php
require_once ('./model/TeamsManager.php');

function showTeam($team_id)
{
    $teamsManager = new TeamsManager();
    $team = $teamsManager->show($team_id);
    $team = json_encode($team);

    echo $team;
}