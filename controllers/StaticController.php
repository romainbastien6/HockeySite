<?php

function home()
{
    require('./views/home.php');
}

function medias()
{
    require('./views/medias.php');
}

function joinus()
{
    require('./views/joinus.php');
}

function results()
{
    require('./views/results.php');
}

function teams()
{
    require('./views/teams.php');
}

function signUpPage()
{
    require ('./views/signup.php');
}

function logInPage()
{
    require ('./views/signin.php');
}
