<?php
    require_once('model/NewsManager.php');
    require_once('model/TeamsManager.php');
    require_once('model/ResultsManager.php');
    require_once('AuthController.php');

    require_once('classes/News.php');
    require_once('classes/Video.php');
    require_once ('classes/Gallery.php');

require __DIR__ . '/../vendor/autoload.php';

    function adminNews()
    {

        $newsManager = new NewsManager();
        $news = $newsManager->indexJSON();
        echo json_encode($news);
    }

    function addNews()
    {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $data = array('id' => 999999, 'title' => $_POST['title'], 'description' => $_POST['description'], 'category' => $_POST['category'], 'featured_image_url' => $_POST['featured_image_url'], 'body' => $_POST['body']);
            if(isset($_POST['title'])){
                $article = new News($data);
                $newsManager = new NewsManager();
                $newsManager->create($article);
            }

    }

    function updateNews()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $data = array('id' => $_POST['id'], 'title' => $_POST['title'], 'description' => $_POST['description'], 'category' => $_POST['category'], 'featured_image_url' => $_POST['featured_image_url'], 'body' => $_POST['body']);
        if(isset($_POST['title'])){
            $article = new News($data);
            $newsManager = new NewsManager();
            $newsManager->update($article);
        }
    }

    function deleteNews()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $news_id = $_POST['id'];
        $newsManager = new NewsManager();
        $newsManager->delete($news_id);
    }

    function adminVideo()
    {
        $videosManager = new VideosManager();
        $videos = $videosManager->indexJSON();
        echo json_encode($videos);
    }

    function updateVideo(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $video = array('id' => $_POST['id'], 'title' => $_POST['title'], 'yt_id' => $_POST['yt_id']);
        $videosManager = new VideosManager();
        $videosManager->update($video);
    }

    function addVideo()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if(isset($_POST['title']))
        {
            $data = array('id' => 999999, 'title' => $_POST['title'], 'yt_id' => $_POST['yt_id']);
            $video = new Video($data);
            $videosManager = new VideosManager();
            $videosManager->create($video);
        }
    }

    function deleteVideo(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $video_id = $_POST['id'];
        $videosManager = new VideosManager();
        $videosManager->delete($video_id);
    }

    function indexGalleries(){
        $imagesManager = new ImagesManager();
        $galleries = $imagesManager->indexJSON();
        echo json_encode($galleries);
    }

    function addGallery()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if (isset($_POST['title']))
        {
            $title = $_POST['title'];
            $gallery = array('id' => 99999, 'title' => $title);
            $gallery = new Gallery($gallery);

            $imagesManager = new ImagesManager();
            $imagesManager->createGallery($gallery);
        }

    }

    function updateGallery()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $gallery = array('id' => $_POST['id'], 'title' => $_POST['title']);
        $imagesManager = new ImagesManager();
        $imagesManager->updateGallery($gallery);
    }

    function addImageToGallery()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $imagesManager = new ImagesManager();
        $imagesManager->addImageToGallery($_POST['gallery_id'], $_POST['img_url']);
    }

    function deleteGallery()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $imagesManager = new ImagesManager();
        $imagesManager->deleteGallery($_POST['id']);
    }

    function deleteImage()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $imagesManager = new ImagesManager();
        $imagesManager->deleteImage($_POST['id']);
    }

    function adminPlayers()
    {
        $TM = new TeamsManager();
        $players = $TM->indexPlayers();
        echo json_encode($players);
    }

    function addPlayer()
    {
        $teamsManager = new TeamsManager();
        $_POST = json_decode(file_get_contents('php://input'), true);
        if (isset($_POST['name']))
        {
            $player = array('name' => $_POST['name'], 'team1_id' => $_POST['team1_id'], 'team2_id' => $_POST['team2_id'], 'position' => $_POST['position'], 'experience' => $_POST['experience'], 'palmares' => $_POST['palmares'], 'picture' => $_POST['picture']);
            $teamsManager->createPlayer($player);
        }
    }

    function updatePlayer()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $teamsManager = new TeamsManager();
        $player = array('id' => $_POST['id'], 'name' => $_POST['name'], 'team1_id' => $_POST['team1_id'], 'team2_id' => $_POST['team2_id'], 'position' => $_POST['position'], 'experience' => $_POST['experience'], 'palmares' => $_POST['palmares'], 'picture' => $_POST['picture']);
        $teamsManager->updatePlayer($player);
    }

    function addPalmares()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $teamsManager = new TeamsManager();
        $teamsManager->addPalmares($_POST['content'], $_POST['player_id']);
    }

    function deletePalmares()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $teamsManager = new TeamsManager();
        $teamsManager->deletePalmares($_POST['id']);
    }


    function adminGames()
    {
        $resultsManager = new ResultsManager();
        $games = $resultsManager->index();
        echo json_encode($games);
    }

    function addGame()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);

        if(isset($_POST['gameType']))
        {
            $resultsManager = new ResultsManager();

            $game = array('gameType' => $_POST['gameType'], 'date' => $_POST['date'], 'team_id' => $_POST['team_id'], 'team1' => $_POST['team1'], 'team2' => $_POST['team2'], 'completed' => $_POST['completed'], 'score1' => $_POST['score1'], 'score2' => $_POST['score2']);

            $resultsManager->createGame($game);
        }

    }

    function updateGame()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $game = array('id' => $_POST['id'], 'gameType' => $_POST['gameType'], 'date' => $_POST['date'], 'team_id' => $_POST['team_id'], 'team1' => $_POST['team1'], 'team2' => $_POST['team2'], 'completed' => $_POST['completed'], 'score1' => $_POST['score1'], 'score2' => $_POST['score2']);
        $resultsManager = new ResultsManager();
        $resultsManager->updateGame($game);
    }

    function deleteGame(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $resultsManager = new ResultsManager();
        $resultsManager->deleteGame($_POST['id']);
    }

    function adminStandings()
    {
        $teamsManager = new TeamsManager();
        $teams = $teamsManager->index();
        echo json_encode($teams);
    }


    function getStandingsTeam($team_id)
    {
        $resultsManager = new ResultsManager();
        $standing = $resultsManager->getTeamStandings($team_id);
        echo json_encode($standing);
    }

    function addStandingsTeam()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if(isset($_POST['teamName']))
        {
            $team = array('category' => $_POST["category"], 'teamName' => $_POST["teamName"], 'playedGames' => $_POST["playedGames"], 'points' => $_POST["points"], 'goalFor' => $_POST["goalFor"], 'goalAgainst' => $_POST["goalAgainst"]);
            $resultsManager = new ResultsManager();
            $id = $resultsManager->addTeamStandings($team);
            return json_encode($id);
        }
    }

    function updateStandingsTeam()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $team = array('id' => $_POST["id"], 'teamName' => $_POST["teamName"], 'playedGames' => $_POST["playedGames"], 'points' => $_POST["points"], 'goalFor' => $_POST["goalFor"], 'goalAgainst' => $_POST["goalAgainst"]);
        $resultsManager = new ResultsManager();
        $resultsManager->updateTeamStandings($team);
    }

    function deleteStandingsTeam()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $team_id = $_POST["id"];
        $resultsManager = new ResultsManager();
        $resultsManager->deleteTeamStandings($team_id);
    }