<?php

class News
{
    private $_id;
    private $_title;
    private $_description;
    private $_category;
    private $_featured_image_url;
    private $_body;



    public function __construct(array $data)
    {
        foreach ($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }

    // Getters

    public function id()
    {
        return $this->_id;
    }

    public function title()
    {
        return $this->_title;
    }

    public function description()
    {
        return $this->_description;
    }

    public function category()
    {
        return $this->_category;
    }

    public function featured_image_url()
    {
        return $this->_featured_image_url;
    }

    public function body()
    {
        return $this->_body;
    }


    // Setters

    public function setId($id)
    {
        $id = (int) $id;

        if ($id > 0)
        {
            $this->_id = $id;
        }
    }

    public function setTitle($title)
    {
        if(is_string($title))
        {
            $this->_title = $title;
        }
    }

    public function setDescription($description)
    {
        if(is_string($description))
        {
            $this->_description = $description;
        }
    }

    public function setCategory($category)
    {
        if(is_string($category))
        {
            $this->_category = $category;
        }
    }

    public function setFeatured_image_url($featured_image_url)
    {
        if(is_string($featured_image_url))
        {
            $this->_featured_image_url = $featured_image_url;
        }
    }

    public function setBody($body)
    {
        if(is_string($body))
        {
            $this->_body = $body;
        }
    }



}