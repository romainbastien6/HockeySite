<?php

class Gallery
{
    private $_id;
    private $_title;
    public $_images = array();

    public function __construct(array $data)
    {
        foreach ($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }

    public function id()
    {
        return $this->_id;
    }

    public function title()
    {
        return $this->_title;
    }

    public function images()
    {
        return $this->_images;
    }

    public function getFirstImage()
    {
        $images = $this->images();
        return $images[0];
    }


    public function setId($id)
    {
        $this->_id = $id;
    }

    public function setTitle($title)
    {
        $this->_title = $title;
    }

    public function setImages($images)
    {
        foreach($images as $image)
        {
            array_push($this->_images, $image);
        }
    }


}