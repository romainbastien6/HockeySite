<?php

class Video
{

    public $_id;
    public $_title;
    public $_yt_id;

    public function __construct(array $data)
    {
        foreach ($data as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }


    public function id()
    {
        return $this->_id;
    }


    public function title()
    {
        return $this->_title;
    }


    public function yt_id()
    {
        return $this->_yt_id;
    }




    public function setId($id)
    {
        $this->_id = $id;
    }


    public function setTitle($title)
    {
        $this->_title = $title;
    }


    public function setYt_id($yt_id)
    {
        $this->_yt_id = $yt_id;
    }


}